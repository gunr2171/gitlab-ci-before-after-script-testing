Tests how `before_script` and `after_script` work when you have different exit codes in the those elements and/or the main `script` section.

## What was learned

* The `after_script` section DOES act as a `finally` block in c# - it will always execute after the main `script` no matter what, success or failure.
* `after_script` runs in a new shell, so it doesn't have access to an `export` variables you might have done. You can't pass information between the sections that way.
* The `after_script` section does, however, have access to the same file system as the previous script sections. This means you can write a file in `before_script` or `script` and read it from `after_script`. This seems like the next best thing to pass simple information around.
